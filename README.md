# German translation stuff for mailman3-suite
This repo contains some .po files with informal German translation. For Postorious you can use the formal one already integrated in the main project.

Use the templates if you want. They add the possibility to add a imprint (Impressum) as well as remove the big icon and the footer with the HyperKitty version. Styles there aren't perfect.

## License
This repo is CC0 licensed where another license doesn't conflict. The templates are mainly taken from their main projects and a bit modified. For the license for those, see the original project.

# Instructions
put the po files to the following structure:
`[somefolder]/locale/de/LC_MESSAGES/django.po`
You might want to combine the po files. Otherwise, use a subdir for postorius and hyperkitty.

In your settings.py file specify the location of the templates and the locales:

```python
LOCALE_PATHS = (
        '/onedir/for/hyperkitty/',
        '/for/postorius/',
        '/for/django-mailman3/',
)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
                BASE_DIR + '/templates',
        ],
...
```